<?php
    
    class Caja extends Connection {
        private $id_caja;
        private $Nombre_de_la_Caja;
        private $NIT;
        private $telefono;
        private $Codigo;
        
        public function __construct($id_caja, $Nombre_de_la_Caja, $NIT, $telefono, $Codigo) {
            $this->id_caja = $id_caja;
            $this->Nombre_de_la_Caja = $Nombre_de_la_Caja;
            $this->NIT = $NIT;
            $this->telefono = $telefono;
            $this->Codigo = $Codigo;
        }

        public function getid_caja() {
            return $this->id_caja;
        }

        public function getNombre_de_la_Caja() {
            return $this->Nombre_de_la_Caja;
        }

        public function getNIT() {
            return $this->NIT;
        }

        public function gettelefono() {
            return $this->telefono;
        }

        public function getCodigo() {
            return $this->Codigo;
        }

        public function get_data() {
            $sql = "SELECT * FROM caja WHERE id_caja = " . $this->getid_caja() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO caja (id_caja, Nombre_de_la_Caja, NIT, telefono, Codigo) VALUES (NULL, '" . $this->getNombre_de_la_Caja() . "', '" . $this->getNIT() . "', '" . $this->gettelefono() . "', '" . $this->getCodigo() . "');";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE caja SET Nombre_de_la_Caja = '" . $this->getNombre_de_la_Caja() . "', NIT = '" . $this->getNIT() . "', telefono = '" . $this->gettelefono() . "', Codigo = '" . $this->getCodigo() . "' WHERE id_caja = " . $this->getid_caja() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM caja";
            $conn = new Connection();
            $conn->select($sql);
            $caja = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($caja, array(
                        "id_caja" => $row->id_caja,
                        "Nombre_de_la_Caja" => $row->Nombre_de_la_Caja,
                        "NIT" => $row->NIT,
                        "telefono" => $row->telefono,
                        "Codigo" => $row->Codigo
                    ));
                }
            }
            return $caja;
        }
    }
?>