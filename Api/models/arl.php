<?php
    
    class Arl extends Connection {
        private $id_arl;
        private $Nombre_de_la_ARL;
        private $NIT;
        private $telefono;
        private $Codigo;

        public function __construct($id_arl, $Nombre_de_la_ARL, $NIT, $telefono, $Codigo) {
            $this->id_arl = $id_arl;
            $this->Nombre_de_la_ARL = $Nombre_de_la_ARL;
            $this->NIT = $NIT;
            $this->telefono = $telefono;
            $this->Codigo = $Codigo;
        }

        public function getid_arl() {
            return $this->id_arl;
        }

        public function getNombre_de_la_ARL() {
            return $this->Nombre_de_la_ARL;
        }

        public function getNIT() {
            return $this->NIT;
        }

        public function gettelefono() {
            return $this->telefono;
        }

        public function getCodigo() {
            return $this->Codigo;
        }

        public function get_data() {
            $sql = "SELECT * FROM arl WHERE id_arl =
            " . $this->getid_arl() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO arl (id_arl, Nombre_de_la_ARL, NIT, telefono, Codigo) 
            VALUES (NULL, '" . 
            $this->getNombre_de_la_ARL() . "', '" . 
            $this->getNIT() . "', '" . 
            $this->gettelefono() . "', '" . 
            $this->getCodigo() . "');";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE arl SET Nombre_de_la_ARL
            = '" . $this->getNombre_de_la_ARL() . 
            "', NIT = '" . $this->getNIT() . "', 
            telefono = '" . $this->gettelefono() . 
            "', Codigo = '" . $this->getCodigo() . 
            "' WHERE id_arl = " . $this->getid_arl() 
            . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM arl";
            $conn = new Connection();
            $conn->select($sql);
            $arl = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($arl, array(
                        "id_arl" => $row->id_arl,
                        "Nombre_de_la_ARL" => $row->Nombre_de_la_ARL,
                        "NIT" => $row->NIT,
                        "telefono" => $row->telefono,
                        "Codigo" => $row->Codigo
                    ));
                }
            }
            return $arl;
        }
        
    }
?>