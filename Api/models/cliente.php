<?php
    
    class Cliente extends Connection {
        private $id_Cliente;
        private $Nombre;
        private $Apellidos;
        private $Tipo_de_Documento;
        private $Numero_de_documento;
        private $Lugar_de_expedicion;
        private $Telefono;
        private $Direccion_de_residencia;
        private $Tipo_de_sangre;
        private $Beneficiarios;
        private $id_plan;
        private $id_eps;
        private $id_arl;
        private $id_afp;
        private $id_caja;

        public function __construct(
            $id_Cliente,
            $Nombre,
            $Apellidos,
            $Tipo_de_Documento,
            $Numero_de_documento,
            $Lugar_de_expedicion,
            $Telefono,
            $Direccion_de_residencia,
            $Tipo_de_sangre,
            $Beneficiarios,
            $id_plan,
            $id_eps,
            $id_arl,
            $id_afp,
            $id_caja
        ) {
            $this->id_Cliente = $id_Cliente;
            $this->Nombre = $Nombre;
            $this->Apellidos = $Apellidos;
            $this->Tipo_de_Documento = $Tipo_de_Documento;
            $this->Numero_de_documento = $Numero_de_documento;
            $this->Lugar_de_expedicion = $Lugar_de_expedicion;
            $this->Telefono = $Telefono;
            $this->Direccion_de_residencia = $Direccion_de_residencia;
            $this->Tipo_de_sangre = $Tipo_de_sangre;
            $this->Beneficiarios = $Beneficiarios;
            $this->id_plan = $id_plan;
            $this->id_eps = $id_eps;
            $this->id_arl = $id_arl;
            $this->id_afp = $id_afp;
            $this->id_caja = $id_caja;
            
        }

        public function getid_Cliente() {
            return $this->id_Cliente;
        }

        public function getNombre() {
            return $this->Nombre;
        }

        public function getApellidos() {
            return $this->Apellidos;
        }

        public function getTipo_de_Documento() {
            return $this->Tipo_de_Documento;
        }

        public function getNumero_de_documento() {
            return $this->Numero_de_documento;
        }

        public function getLugar_de_expedicion		() {
            return $this->Lugar_de_expedicion;
        }

        public function getTelefono	() {
            return $this->Telefono;
        }

        public function getDireccion_de_residencia() {
            return $this->Direccion_de_residencia;
        }

        public function getTipo_de_sangre() {
            return $this->Tipo_de_sangre;
        }

        public function getBeneficiarios() {
            return $this->Beneficiarios	;
        }

        public function getid_plan() {
            return $this->id_plan;
        }

        public function getid_eps() {
            return $this->id_eps;
        }

        public function getid_arl() {
            return $this->id_arl;
        }

        public function getid_afp() {
            return $this->id_afp;
        }

        public function getid_caja() {
            return $this->id_caja;
        }

        public function get_data() {
            $sql = "SELECT * FROM cliente WHERE id_Cliente = " . $this->getid_Cliente() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO cliente (
                id_Cliente, 
                Nombre, 
                Apellidos, 
                Tipo_de_Documento, 
                Numero_de_documento, 
                Lugar_de_expedicion, 
                Telefono, 
                Direccion_de_residencia, 
                Tipo_de_sangre, 
                Beneficiarios,
                id_plan,
                id_eps,
                id_arl,
                id_afp,
                id_caja) VALUES (NULL, '"  
                . $this->getNombre() . "', '" 
                . $this->getApellidos() . "', '" 
                . $this->getTipo_de_Documento() . "', "  
                . $this->getNumero_de_documento() . ", '" 
                . $this->getLugar_de_expedicion() . "', '" 
                . $this->getTelefono() . "', '" 
                . $this->getDireccion_de_residencia() . "', '" 
                . $this->getTipo_de_sangre() . "', '"
                . $this->getBeneficiarios() . "', "
                . $this->getid_plan() . ", "
                . $this->getid_eps() . ", "
                . $this->getid_arl() . ", "
                . $this->getid_afp() . ", " 
                . $this->getid_caja() . ");";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE cliente SET 
            Nombre = '" . $this->getNombre() .
            "', Apellidos = '" . $this->getApellidos() . 
            "', Tipo_de_Documento = '" . $this->getTipo_de_Documento() . 
            "', Numero_de_documento = '" . $this->getNumero_de_documento() . 
            "', Lugar_de_expedicion = '" . $this->getLugar_de_expedicion() .
            "', Telefono = '" . $this->getTelefono() .
            "', Direccion_de_residencia = '" . $this->getDireccion_de_residencia() .
            "', Tipo_de_sangre = '" . $this->getTipo_de_sangre() . 
            "', Beneficiarios = '" . $this->getBeneficiarios() .
            "', id_plan = '" . $this->getid_plan() .
            "', id_eps = '" . $this->getid_eps() .
            "', id_arl	 = '" . $this->getid_arl	() .
            "', id_afp = '" . $this->getid_afp() . 
            "', id_caja = '" . $this->getid_caja() . 
            "' WHERE id_Cliente = " . 
            $this->getid_Cliente() . ";";
            
            $this->select($sql);
            return 
            $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM cliente";
            $conn = new Connection();
            $conn->select($sql);
            $cliente = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($cliente, array(
                        "id_Cliente" => $row->id_Cliente,
                        "Nombre" => $row->Nombre,
                        "Apellidos" => $row->Apellidos,
                        "Tipo_de_Documento" => $row->Tipo_de_Documento,
                        "Numero_de_documento" => $row->Numero_de_documento,
                        "Lugar_de_expedicion" => $row->Lugar_de_expedicion,
                        "Telefono" => $row->Telefono,
                        "Direccion_de_residencia" => $row->Direccion_de_residencia,
                        "Tipo_de_sangre" => $row->Tipo_de_sangre,
                        "Beneficiarios" => $row->Beneficiarios,
                        "id_plan" => $row->id_plan,
                        "id_eps" => $row->id_eps,
                        "id_arl	" => $row->id_arl,
                        "id_afp" => $row->id_afp,
                        "id_caja" => $row->id_caja
                    ));
                }
            }
            return $cliente;
        }
    }
?>