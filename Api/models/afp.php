<?php

    class Afp extends Connection {
        private $id_afp;
        private $Nombre_de_la_AFP;
        private $NIT;
        private $telefono;
        private $Codigo;
        
        public function __construct($id_afp, $Nombre_de_la_AFP, $NIT, $telefono, $Codigo) {
            $this->id_afp = $id_afp;
            $this->Nombre_de_la_AFP = $Nombre_de_la_AFP;
            $this->NIT = $NIT;
            $this->telefono = $telefono;
            $this->Codigo = $Codigo;
        }

        public function getid_afp() {
            return $this->id_afp;
        }

        public function getNombre_de_la_AFP() {
            return $this->Nombre_de_la_AFP;
        }

        public function getNIT() {
            return $this->NIT;
        }

        public function gettelefono() {
            return $this->telefono;
        }

        public function getCodigo() {
            return $this->Codigo;
        }

        public function get_data() {
            $sql = "SELECT * FROM afp WHERE id_afp = " . $this->getid_afp() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO afp (id_afp, Nombre_de_la_AFP, NIT, telefono, Codigo) VALUES (NULL, '" . $this->getNombre_de_la_AFP() . "', '" . $this->getNIT() . "', '" . $this->gettelefono() . "', '" . $this->getCodigo() . "');";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE afp SET Nombre_de_la_AFP = '" . $this->getNombre_de_la_AFP() . "', NIT = '" . $this->getNIT() . "', telefono = '" . $this->gettelefono() . "', Codigo = '" . $this->getCodigo() . "' WHERE id_afp = " . $this->getid_afp() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM afp";
            $conn = new Connection();
            $conn->select($sql);
            $afp = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($afp, array(
                        "id_afp" => $row->id_afp,
                        "Nombre_de_la_AFP" => $row->Nombre_de_la_AFP,
                        "NIT" => $row->NIT,
                        "telefono" => $row->telefono,
                        "Codigo" => $row->Codigo
                    ));
                }
            }
            return $afp;
        }
    }
?>