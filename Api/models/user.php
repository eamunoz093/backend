<?php
    class User extends Connection {
        private $id_Usuario;
        private $id_rol;
        private $Nombre;
        private $Apellidos;
        private $Tipo_de_documento;
        private $Numero_de_documento;
        private $Telefono;
        private $Direccion_de_residencia;
        private $Correo_electronico;
        private $Usuario;
        private $Contrasena;

        public function __construct($Usuario, $Contrasena) {
            $this->Usuario = $Usuario;
            $this->Contrasena = $Contrasena;
        }

        public function getUsuario() {
            return $this->Usuario;
        }

        public function getContrasena() {
            return $this->Contrasena;
        }

        public function getid_Usuario(){
            return $this->id_Usuario;
        }

        public function getid_rol(){
            return $this->id_rol;
        }

        public function getNombre(){
            return $this->Nombre;
        }

        public function getApellidos(){
            return $this->Apellidos;
        }

        public function getTipo_de_documento(){
            return $this->Tipo_de_documento;
        }

        public function getNumero_de_documento(){
            return $this->Numero_de_documento;
        }

        public function getTelefono(){
            return $this->Telefono;
        }

        public function getDireccion_de_residencia(){
            return $this->Direccion_de_residencia;
        }

        public function getCorreo_electronico(){
            return $this->Correo_electronico;
        }

        public function login() {
            $sql = "SELECT * FROM usuarios WHERE Usuario = '" . $this->getUsuario() . "' AND Contrasena = '" . $this->getContrasena() . "'";
            $this->select($sql);
            return $this->getResult();
        }
    }
?>