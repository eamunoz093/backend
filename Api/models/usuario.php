<?php

    class Usuario extends Connection {
        private $id_Usuario;
        private $id_rol;
        private $Nombre;
        private $Apellidos;
        private $Tipo_de_documento;
        private $Numero_de_documento;
        private $Telefono;
        private $Direccion_de_residencia;
        private $Correo_electronico;
        private $Usuario;
        private $Contrasena;
        
        
        public function __construct($id_Usuario, $id_rol, $Nombre, $Apellidos, $Tipo_de_documento, $Numero_de_documento, $Telefono, $Direccion_de_residencia, $Correo_electronico, $Usuario, $Contrasena  ) {
            $this->id_Usuario = $id_Usuario;
            $this->id_rol = $id_rol;
            $this->Nombre = $Nombre;
            $this->Apellidos = $Apellidos;
            $this->Tipo_de_documento = $Tipo_de_documento;
            $this->Numero_de_documento = $Numero_de_documento;
            $this->Telefono = $Telefono;
            $this->Direccion_de_residencia = $Direccion_de_residencia;
            $this->Correo_electronico = $Correo_electronico;
            $this->Usuario = $Usuario;
            $this->Contrasena = $Contrasena;
            
        }

        public function getid_Usuario() {
            return $this->id_Usuario;
        }

        public function getid_rol() {
            return $this->id_rol;
        }

        public function getNombre() {
            return $this->Nombre;
        }

        public function getApellidos() {
            return $this->Apellidos;
        }

        public function getTipo_de_documento() {
            return $this->Tipo_de_documento;
        }

        public function getNumero_de_documento() {
            return $this->Numero_de_documento;
        }

        public function getTelefono() {
            return $this->Telefono;
        }

        public function getDireccion_de_residencia() {
            return $this->Direccion_de_residencia;
        }

        public function getCorreo_electronico() {
            return $this->Correo_electronico;
        }

        public function getUsuario() {
            return $this->Usuario;
        }

        public function getContrasena() {
            return $this->Contrasena;
        }

        public function get_data() {
            $sql = "SELECT * FROM usuarios WHERE id_Usuario = " . $this->getid_Usuario() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO usuarios (id_Usuario, id_rol, Nombre, Apellidos, Tipo_de_documento, Numero_de_documento, Telefono, Direccion_de_residencia, Correo_electronico, Usuario, Contrasena) VALUES (NULL, " . $this->getid_rol() . ", '" . $this->getNombre() . "', '" . $this->getApellidos() . "', '" . $this->getTipo_de_documento() . "', "  . $this->getNumero_de_documento() . ", " . $this->getTelefono() . ", '" . $this->getDireccion_de_residencia() . "', '" . $this->getCorreo_electronico() . "', '" . $this->getUsuario() . "', '" . $this->getContrasena() . "');";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE usuarios SET id_rol = 
            '" . $this->getid_rol() .
            "' Nombre = '" . $this->getNombre() .
            "', Apellidos = '" . $this->getApellidos() . 
            "', Tipo_de_documento = '" . $this->getTipo_de_documento() . 
            "', Numero_de_documento = '" . $this->getNumero_de_documento() . 
            "', Telefono = '" . $this->getTelefono() .
            "', Direccion_de_residencia = '" . $this->getDireccion_de_residencia() .
            "', Correo_electronico = '" . $this->getCorreo_electronico() .
            "', Usuario = '" . $this->getUsuario() . 
            "', Contrasena = '" . $this->getContrasena() . 
            "', Telefono = '" . $this->getTelefono() . 
            "' WHERE id_Usuario = " . 
            $this->getid_Usuario() . ";";
            
            $this->select($sql);
            return 
            $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM usuarios";
            $conn = new Connection();
            $conn->select($sql);
            $usuarios = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($usuarios, array(
                        "id_Usuario" => $row->id_Usuario,
                        "Nombre" => $row->Nombre,
                        "Apellidos" => $row->Apellidos,
                        "Tipo_de_documento" => $row->Tipo_de_documento,
                        "Numero_de_documento" => $row->Numero_de_documento,
                        "Usuario" => $row->Usuario,
                        "Contrasena" => $row->Contrasena,
                        "Telefono" => $row->Telefono
                    ));
                }
            }
            return $usuarios;
        }
    }
?>