<?php
class Connection {
  private $servername = "localhost";
  private $username = "root";
  private $password = "";
  private $db = "avanzarcompany";

  private $conn = null;

  private $sql = null;
  private $result = null;

  public function __construct() {

  }

  public function connect() {
    // Create connection
    $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->db);

    // Check connection
    if ($this->conn->connect_errno) {
      echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
      exit();
    }
  }

  public function close() {
    $this->conn->close();
  }

  public function getConn() {
    return $this->conn;
  }

  public function getSql() {
    return $this->sql;
  }

  public function setSql($sql) {
    return $this->sql = $sql;
  }

  public function getResult() {
    return $this->result;
  }

  public function select($sql) {
    $this->connect();
    $this->result = $this->getConn()->query($sql);
    if(!$this->getResult()) {
      var_dump($this->getConn()->error);
    }
    $this->close();
  }
}
?>