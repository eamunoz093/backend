<?php
    class Eps extends Connection {
        private $id_eps;
        private $Nombre_de_la_EPS;
        private $NIT;
        private $telefono;
        private $Codigo;

        public function __construct($id_eps, $Nombre_de_la_EPS, $NIT, $telefono, $Codigo) {
            $this->id_eps = $id_eps;
            $this->Nombre_de_la_EPS = $Nombre_de_la_EPS;
            $this->NIT = $NIT;
            $this->telefono = $telefono;
            $this->Codigo = $Codigo;
        }

        public function getid_eps() {
            return $this->id_eps;
        }

        public function getNombre_de_la_EPS() {
            return $this->Nombre_de_la_EPS;
        }

        public function getNIT() {
            return $this->NIT;
        }

        public function gettelefono() {
            return $this->telefono;
        }

        public function getCodigo() {
            return $this->Codigo;
        }

        public function get_data() {
            $sql = "SELECT * FROM eps WHERE id_eps = " . $this->getid_eps() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public function save() {
            $sql = "INSERT INTO eps (id_eps, Nombre_de_la_EPS, NIT, telefono, Codigo) VALUES (NULL, '" . $this->getNombre_de_la_EPS() . "', '" . $this->getNIT() . "', '" . $this->gettelefono() . "', '" . $this->getCodigo() . "');";
            $this->select($sql);
            return $this->getResult();
        }

        public function update() {
            $sql = "UPDATE eps SET Nombre_de_la_EPS = '" . $this->getNombre_de_la_EPS() . "', NIT = '" . $this->getNIT() . "', telefono = '" . $this->gettelefono() . "', Codigo = '" . $this->getCodigo() . "' WHERE id_eps = " . $this->getid_eps() . ";";
            $this->select($sql);
            return $this->getResult();
        }

        public static function get_all() {
            $sql = "SELECT * FROM eps";
            $conn = new Connection();
            $conn->select($sql);
            $eps = array();
            if($conn->getResult()) {
                while ($row = $conn->getResult()->fetch_object()){
                    array_push($eps, array(
                        "id_eps" => $row->id_eps,
                        "Nombre_de_la_EPS" => $row->Nombre_de_la_EPS,
                        "NIT" => $row->NIT,
                        "telefono" => $row->telefono,
                        "Codigo" => $row->Codigo
                    ));
                }
            }
            return $eps;
        }
    }

    // Testing
?>