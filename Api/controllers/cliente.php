<?php
    require_once("../models/connection.php");
    require_once("../models/cliente.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $cliente = Cliente::get_all();
        if($cliente) {
            $jsonEncode = json_encode(array("status" => "success", "cliente" => $cliente));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "get_by_id") {
        if(isset($_GET["id_Cliente"])) {
            $cliente = new Cliente($_GET["id_Cliente"], null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            if($cliente->get_data()->num_rows > 0) {
                $jsonEncode = json_encode(array("status" => "success", "cliente" => $cliente->get_data()->fetch_assoc()));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        }else{
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "create") {
        if(
            isset($_POST["Nombre"])
            && isset($_POST["Apellidos"])
            && isset($_POST["Tipo_de_Documento"])
            && isset($_POST["Numero_de_documento"])
            && isset($_POST["Lugar_de_expedicion"])
            && isset($_POST["Telefono"])
            && isset($_POST["Direccion_de_residencia"])
            && isset($_POST["Tipo_de_sangre"])
            && isset($_POST["Beneficiarios"])
            && isset($_POST["id_plan"])
            && isset($_POST["id_eps"])
            && isset($_POST["id_arl"])
            && isset($_POST["id_afp"])
            && isset($_POST["id_caja"])
        ) {
            $cliente = new Cliente(
                null,
                $_POST["Nombre"], 
                $_POST["Apellidos"], 
                $_POST["Tipo_de_Documento"], 
                $_POST["Numero_de_documento"], 
                $_POST["Lugar_de_expedicion"], 
                $_POST["Telefono"], 
                $_POST["Direccion_de_residencia"], 
                $_POST["Tipo_de_sangre"], 
                $_POST["Beneficiarios"],
                $_POST["id_plan"],
                $_POST["id_eps"],
                $_POST["id_arl"],
                $_POST["id_afp"],
                $_POST["id_caja"]
            );
            if($cliente->save()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed save"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed receive data"));
        }
    } elseif ($method == "update") {
            if(
                isset($_POST["id_Cliente"]) &&  
                isset($_POST["Nombre"]) && 
                isset($_POST["Apellidos"]) && 
                isset($_POST["Tipo_de_Documento"]) && 
                isset($_POST["Numero_de_documento"]) &&
                isset($_POST["Lugar_de_expedicion"]) &&
                isset($_POST["Telefono"]) &&
                isset($_POST["Direccion_de_residencia"]) && 
                isset($_POST["Tipo_de_sangre"]) && 
                isset($_POST["Beneficiarios"]) && 
                isset($_POST["id_plan"]) && 
                isset($_POST["id_eps"]) && 
                isset($_POST["id_arl"]) && 
                isset($_POST["id_afp"]) && 
                isset($_POST["id_caja"])
            ) {
                $cliente = new Cliente(
                    $_POST["id_Cliente"], 
                    $_POST["Nombre"], 
                    $_POST["Apellidos"], 
                    $_POST["Tipo_de_Documento"], 
                    $_POST["Numero_de_documento"],
                    $_POST["Lugar_de_expedicion"],
                    $_POST["Telefono"],
                    $_POST["Direccion_de_residencia"], 
                    $_POST["Tipo_de_sangre"], 
                    $_POST["Beneficiarios"],
                    $_POST["id_plan"],
                    $_POST["id_eps"],
                    $_POST["id_arl"],
                    $_POST["id_afp"],
                    $_POST["id_caja"]
                );
                if($cliente->update()) {
                    $jsonEncode = json_encode(array("status" => "success"));
                } else {
                    $jsonEncode = json_encode(array("status" => "failed update "));
                }
            } else {
                $jsonEncode = json_encode(array("status" => "failed receive data"));
            }
    }
    echo $jsonEncode;
?>