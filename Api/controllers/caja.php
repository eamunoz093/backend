<?php
    require_once("../models/connection.php");
    require_once("../models/caja.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $caja = Caja::get_all();
        if($caja) {
            $jsonEncode = json_encode(array("status" => "success", "caja" => $caja));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "get_by_id") {
        if(isset($_GET["id_caja"])) {
            $caja = new Caja($_GET["id_caja"], null, null, null, null);
            if($caja->get_data()->num_rows > 0) {
                $jsonEncode = json_encode(array("status" => "success", "caja" => $caja->get_data()->fetch_assoc()));
            } else {
                $jsonEncode = json_encode(array("login" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
        } elseif ($method == "create") {
            if(isset($_POST["Nombre_de_la_Caja"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
                $caja = new Caja(null, $_POST["Nombre_de_la_Caja"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
                if($caja->save()) {
                    $jsonEncode = json_encode(array("status" => "success"));
                } else {
                    $jsonEncode = json_encode(array("status" => "failed"));
                }
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
    } elseif ($method == "update") {
        if(isset($_POST["id_caja"]) && isset($_POST["Nombre_de_la_Caja"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
            $caja = new Caja($_POST["id_caja"], $_POST["Nombre_de_la_Caja"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
            if($caja->update()) {
                    $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    }
    echo $jsonEncode;
?>