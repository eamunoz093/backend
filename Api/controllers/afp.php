<?php
    require_once("../models/connection.php");
    require_once("../models/afp.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $afp = Afp::get_all();
        if($afp) {
            $jsonEncode = json_encode(array("status" => "success", "afp" => $afp));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    }elseif ($method == "get_by_id") {
        if(isset($_GET["id_afp"])) {
            $afp = new Afp($_GET["id_afp"], null, null, null, null);
            if($afp->get_data()->num_rows > 0) {
                $jsonEncode = json_encode(array("status" => "success", "afp" => $afp->get_data()->fetch_assoc()));
            } else {
                $jsonEncode = json_encode(array("login" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    }elseif ($method == "create") {
        if(isset($_POST["Nombre_de_la_AFP"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
            $afp = new Afp(null, $_POST["Nombre_de_la_AFP"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
            if($afp->save()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "update") {
        if(isset($_POST["id_afp"]) && isset($_POST["Nombre_de_la_AFP"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
            $afp = new Afp($_POST["id_afp"], $_POST["Nombre_de_la_AFP"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
            if($afp->update()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    }
    echo $jsonEncode;
?>