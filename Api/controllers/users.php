<?php
    require_once("../models/connection.php");
    require_once("../models/user.php");
    $jsonEncode = json_encode(array("login" => "success"));
    $method = $_GET["method"];
    if ($method == "login") {
        if(isset($_POST["user"]) && isset($_POST["pass"])) {
            $user = new User($_POST["user"], $_POST["pass"]);
            if($user->login()->num_rows > 0) {
                $jsonEncode = json_encode(array("login" => "success"));
            } else {
                $jsonEncode = json_encode(array("login" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("login" => "failed"));
        }
    }
    echo $jsonEncode;
?>