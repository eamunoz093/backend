<?php
    require_once("../models/connection.php");
    require_once("../models/eps.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $eps = Eps::get_all();
        if($eps) {
            $jsonEncode = json_encode(array("status" => "success", "eps" => $eps));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "get_by_id") {
        if(isset($_GET["id_eps"])) {
            $eps = new Eps($_GET["id_eps"], null, null, null, null);
            if($eps->get_data()->num_rows > 0) {
                $jsonEncode = json_encode(array("status" => "success", "eps" => $eps->get_data()->fetch_assoc()));
            } else {
                $jsonEncode = json_encode(array("login" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "create") {
        if(isset($_POST["Nombre_de_la_EPS"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
            $eps = new Eps(null, $_POST["Nombre_de_la_EPS"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
            if($eps->save()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "update") {
        if(isset($_POST["id_eps"]) && isset($_POST["Nombre_de_la_EPS"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
            $eps = new Eps($_POST["id_eps"], $_POST["Nombre_de_la_EPS"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
            if($eps->update()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    }
    echo $jsonEncode;
?>