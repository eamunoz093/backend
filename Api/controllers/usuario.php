<?php
    require_once("../models/connection.php");
    require_once("../models/usuario.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $usuario = Usuario::get_all();
        if($usuario) {
            $jsonEncode = json_encode(array("status" => "success", "usuario" => $usuario));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "get_by_id") {
        if(isset($_GET["id_Usuario"])) {
            $usuario = new Usuario($_GET["id_Usuario"], null, null, null, null, null, null, null, null, null, null);
            if($usuario->get_data()->num_rows > 0) {
                $jsonEncode = json_encode(array("status" => "success", "usuario" => $usuario->get_data()->fetch_assoc()));
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        }else{
            $jsonEncode = json_encode(array("status" => "failed"));
        }
    } elseif ($method == "create") {
        if(
            isset($_POST["id_rol"])
            && isset($_POST["Nombre"])
            && isset($_POST["Apellidos"])
            && isset($_POST["Tipo_de_documento"])
            && isset($_POST["Numero_de_documento"])
            && isset($_POST["Telefono"])
            && isset($_POST["Direccion_de_residencia"])
            && isset($_POST["Correo_electronico"])
            && isset($_POST["Usuario"])
            && isset($_POST["Contrasena"])
        ) {
            $usuario = new Usuario(
                null, $_POST["id_rol"], $_POST["Nombre"], $_POST["Apellidos"], $_POST["Tipo_de_documento"], $_POST["Numero_de_documento"], $_POST["Telefono"], $_POST["Direccion_de_residencia"] , $_POST["Correo_electronico"], $_POST["Usuario"], $_POST["Contrasena"]
            );
            if($usuario->save()) {
                $jsonEncode = json_encode(array("status" => "success"));
            } else {
                $jsonEncode = json_encode(array("status" => "failed save"));
            }
        } else {
            $jsonEncode = json_encode(array("status" => "failed receive data"));
        }
    } elseif ($method == "update") {
            if(
                isset($_POST["id_Usuario"]) &&
                isset($_POST["id_rol"]) && 
                isset($_POST["Nombre"]) && 
                isset($_POST["Apellidos"]) && 
                isset($_POST["Tipo_de_documento"]) && 
                isset($_POST["Numero_de_documento"]) &&
                isset($_POST["Telefono"]) &&
                isset($_POST["Direccion_de_residencia"]) &&
                isset($_POST["Correo_electronico"]) && 
                isset($_POST["Usuario"]) && 
                isset($_POST["Contrasena"])
            ) {
                $usuario = new Usuario(
                    $_POST["id_Usuario"],
                    $_POST["id_rol"], 
                $_POST["Nombre"], 
                $_POST["Apellidos"], 
                $_POST["Tipo_de_documento"], 
                $_POST["Numero_de_documento"],
                $_POST["Telefono"],
                $_POST["Direccion_de_residencia"],
                $_POST["Correo_electronico"], 
                $_POST["Usuario"], 
                $_POST["Contrasena"]);
                if($usuario->update()) {
                    $jsonEncode = json_encode(array("status" => "success"));
                } else {
                    $jsonEncode = json_encode(array("status" => "failed update"));
                }
            } else {
                $jsonEncode = json_encode(array("status" => "failed receive data"));
            }
    }
    echo $jsonEncode;
?>