<?php
    require_once("../models/connection.php");
    require_once("../models/arl.php");
    $jsonEncode = json_encode(array());
    $method = $_GET["method"];
    if ($method == "get_all") {
        $arl = Arl::get_all();
        if($arl) {
            $jsonEncode = json_encode(array("status" => "success", "arl" => $arl));
        } else {
            $jsonEncode = json_encode(array("status" => "failed"));
        }
        } elseif ($method == "get_by_id") {
            if(isset($_GET["id_arl"])) {
                $arl = new Arl($_GET["id_arl"], null, null, null, null);
                if($arl->get_data()->num_rows > 0) {
                    $jsonEncode = json_encode(array("status" => "success", "arl" => $arl->get_data()->fetch_assoc()));
                } else {
                    $jsonEncode = json_encode(array("login" => "failed"));
                }
            }else{
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } elseif ($method == "create") {
            if(isset($_POST["Nombre_de_la_ARL"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
                $arl = new Arl(null, $_POST["Nombre_de_la_ARL"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
                if($arl->save()) {
                    $jsonEncode = json_encode(array("status" => "success"));
                } else {
                    $jsonEncode = json_encode(array("status" => "failed"));
                }
            } else {
                $jsonEncode = json_encode(array("status" => "failed"));
            }
        } elseif ($method == "update") {
            if(isset($_POST["id_arl"]) && isset($_POST["Nombre_de_la_ARL"]) && isset($_POST["NIT"]) && isset($_POST["telefono"]) && isset($_POST["Codigo"])) {
                $arl = new Arl($_POST["id_arl"], $_POST["Nombre_de_la_ARL"], $_POST["NIT"], $_POST["telefono"], $_POST["Codigo"]);
                if($arl->update()) {
                    $jsonEncode = json_encode(array("status" => "success"));
                } else {
                    $jsonEncode = json_encode(array("status" => "failed update"));
                }
            } else {
                $jsonEncode = json_encode(array("status" => "failed receive data"));
            }
    }
    echo $jsonEncode;
?>