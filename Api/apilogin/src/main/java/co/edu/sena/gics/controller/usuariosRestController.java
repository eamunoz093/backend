package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.usuarios;
import co.edu.sena.gics.servicesInterface.IusuariosServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})
public class usuariosRestController {
	@Autowired
	private IusuariosServices userService;
	
	@GetMapping("/usuarios")
	public List<usuarios> getAll(){
		return userService.findAll();
	}
	
	@PostMapping("/usuarios")
	public usuarios create(@RequestBody usuarios usuarios) {
		System.out.println(usuarios.toString());
		return userService.create(usuarios);
	}

}
