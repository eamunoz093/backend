package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IRolDao;
import co.edu.sena.gics.entities.Rol;
import co.edu.sena.gics.servicesInterface.IRolServices;

@Service
public class RolServices implements IRolServices{

	@Autowired
	private IRolDao rolDao;
	
	@Override
	public List<Rol> findAll() {
		return (List<Rol>) rolDao.findAll();
	}

	@Override
	public Rol create(Rol rol) {
		return rolDao.save(rol);
	}
	
}

