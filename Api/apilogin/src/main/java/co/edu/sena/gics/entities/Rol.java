package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Rol implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* Atributos */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rol", nullable=false)
	private long id;
	
	@Column(name="tipo_de_rol", nullable=false)
	private String tipo_de_rol;
	
	/*Constructor*/

	public Rol(long id, String tipo_de_rol) {
		super();
		this.id = id;
		this.tipo_de_rol = tipo_de_rol;
	}

	public Rol() {
		super();
	}

	/*Métodos*/
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipo_de_rol() {
		return tipo_de_rol;
	}

	public void setTipo_de_rol(String tipo_de_rol) {
		this.tipo_de_rol = tipo_de_rol;
	}

	@Override
	public String toString() {
		return "Rol [id=" + id + ", tipo_de_rol=" + tipo_de_rol + "]";
	}
	
}

