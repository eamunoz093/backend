package co.edu.sena.gics.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class usuarios implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		/* Atributos */
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id_Usuario", nullable=false)
		private long id;
		
		@ManyToOne
		@JoinColumn(name = "id_rol", nullable=false)
		private Rol id_rol;
		
		@Column(name="Nombre", nullable=false)
		private String Nombre;
		
		@Column(name="Apellidos", nullable=false)
		private String Apellidos;
		
		@Column(name="Tipo_de_documento", nullable=false)
		private String Tipo_de_documento;
		
		@Column(name="Numero_de_documento", nullable=false)
		private String Numero_de_documento;
		
		@Column(name="Telefono", nullable=false)
		private String Telefono;
		
		@Column(name="Direccion_de_residencia", nullable=false)
		private String Direccion_de_residencia;
		
		@Column(name="Correo_electronico", nullable=false)
		private String Correo_electronico;
		
		@Column(name="Usuario", nullable=false)
		private String Usuario;
		
		@Column(name="Contraseña", nullable=false)
		private String Contraseña;
		
		/* Constructotes */

		public usuarios(long id, Rol id_rol, String nombre, String apellidos, String tipo_de_documento,
				String numero_de_documento, String telefono, String direccion_de_residencia, String correo_electronico,
				String usuario, String contraseña) {
			super();
			this.id = id;
			this.id_rol = id_rol;
			this.Nombre = nombre;
			this.Apellidos = apellidos;
			this.Tipo_de_documento = tipo_de_documento;
			this.Numero_de_documento = numero_de_documento;
			this.Telefono = telefono;
			this.Direccion_de_residencia = direccion_de_residencia;
			this.Correo_electronico = correo_electronico;
			this.Usuario = usuario;
			this.Contraseña = contraseña;
		}
		
		/* Metodos */

		public usuarios() {
			super();
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public Rol getId_rol() {
			return id_rol;
		}

		public void setId_rol(Rol id_rol) {
			this.id_rol = id_rol;
		}

		public String getNombre() {
			return Nombre;
		}

		public void setNombre(String nombre) {
			Nombre = nombre;
		}

		public String getApellidos() {
			return Apellidos;
		}

		public void setApellidos(String apellidos) {
			Apellidos = apellidos;
		}

		public String getTipo_de_documento() {
			return Tipo_de_documento;
		}

		public void setTipo_de_documento(String tipo_de_documento) {
			Tipo_de_documento = tipo_de_documento;
		}

		public String getNumero_de_documento() {
			return Numero_de_documento;
		}

		public void setNumero_de_documento(String numero_de_documento) {
			Numero_de_documento = numero_de_documento;
		}

		public String getTelefono() {
			return Telefono;
		}

		public void setTelefono(String telefono) {
			Telefono = telefono;
		}

		public String getDireccion_de_residencia() {
			return Direccion_de_residencia;
		}

		public void setDireccion_de_residencia(String direccion_de_residencia) {
			Direccion_de_residencia = direccion_de_residencia;
		}

		public String getCorreo_electronico() {
			return Correo_electronico;
		}

		public void setCorreo_electronico(String correo_electronico) {
			Correo_electronico = correo_electronico;
		}

		public String getUsuario() {
			return Usuario;
		}

		public void setUsuario(String usuario) {
			Usuario = usuario;
		}

		public String getContraseña() {
			return Contraseña;
		}

		public void setContraseña(String contraseña) {
			Contraseña = contraseña;
		}

		@Override
		public String toString() {
			return "usuarios [id=" + id + ", id_rol=" + id_rol + ", Nombre=" + Nombre + ", Apellidos=" + Apellidos
					+ ", Tipo_de_documento=" + Tipo_de_documento + ", Numero_de_documento=" + Numero_de_documento
					+ ", Telefono=" + Telefono + ", Direccion_de_residencia=" + Direccion_de_residencia
					+ ", Correo_electronico=" + Correo_electronico + ", Usuario=" + Usuario + ", Contraseña="
					+ Contraseña + "]";
		}
		
		
		
	
       	
		
}
