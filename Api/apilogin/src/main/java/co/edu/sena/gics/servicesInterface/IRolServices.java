package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.Rol;

public interface IRolServices {

	public List<Rol> findAll();
	
	public Rol create(Rol rol); 
	
}
