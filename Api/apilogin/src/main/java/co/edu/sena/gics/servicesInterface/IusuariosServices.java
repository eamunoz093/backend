package co.edu.sena.gics.servicesInterface;

import java.util.List;

import co.edu.sena.gics.entities.usuarios;

public interface IusuariosServices {

	public List<usuarios> findAll();
	
	public usuarios create(usuarios user); 
	
}

