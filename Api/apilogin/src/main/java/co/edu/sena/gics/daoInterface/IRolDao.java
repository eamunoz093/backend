package co.edu.sena.gics.daoInterface;

import org.springframework.data.repository.CrudRepository;

import co.edu.sena.gics.entities.Rol;

public interface IRolDao extends CrudRepository<Rol, Long> {

}


