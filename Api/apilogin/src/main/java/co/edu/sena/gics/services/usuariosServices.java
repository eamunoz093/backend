package co.edu.sena.gics.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.edu.sena.gics.daoInterface.IusuariosDao;
import co.edu.sena.gics.entities.usuarios;
import co.edu.sena.gics.servicesInterface.IusuariosServices;

@Service
public class usuariosServices implements IusuariosServices{

	@Autowired
	private IusuariosDao userDao;
	
	@Override
	public List<usuarios> findAll() {
		// TODO Auto-generated method stub
		return (List<usuarios>) userDao.findAll();
	}

	@Override
	public usuarios create(usuarios user) {
		// TODO Auto-generated method stub
		return userDao.save(user);
	}

}
