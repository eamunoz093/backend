package co.edu.sena.gics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.edu.sena.gics.entities.Rol;
import co.edu.sena.gics.servicesInterface.IRolServices;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="*", methods= {RequestMethod.GET, RequestMethod.POST})
public class RolRestController {
	
	@Autowired
	private IRolServices rolService;
	
	@GetMapping("/roles")
	public List<Rol> getAll(){
		return rolService.findAll();
	}
	
	@PostMapping("/roles")
	public Rol create(@RequestBody Rol roles) {
		System.out.println(roles.toString());
		return rolService.create(roles);
	}

}
